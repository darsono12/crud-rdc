<!DOCTYPE html>
<html>
<body>
<div class="container">
    <br>
    <h4>CRUD Sederhana dengan PHP</h4>
    <a href="form_tambah.php">Tambah Data</a>
    <table>
        <br>
        <thead>
            <tr>
                <th>id</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th colspan='2'>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <!-- langkah 1 -->
            <tr>
                <td><!-- langkah 2 --></td>
                <td><!-- langkah 2 --></td>
                <td><!-- langkah 2 --></td>
                <td>
                    <a href="#">Edit</a>
                    <a href="#">Delete</a>
                </td>
            </tr>
            <!-- langkah 3 -->
        </tbody>
    </table>
    
</div>
</body>
</html>
